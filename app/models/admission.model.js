module.exports = (sequelize, Sequelize) => {
  const Admission = sequelize.define("admission", {
    full_name: {
      type: Sequelize.STRING,
      validate:{len: [6,50]}
    },
    mobile: {
      type: Sequelize.INTEGER,
      validate:{len: 11}
    },
    dept: {
      type: Sequelize.STRING,
      validate:{len: 3}
    },
    ssc_point: {
      type: Sequelize.FLOAT,
      validate:{
          min: 2.00,
          max: 5.00
      }
    },
    hsc_point: {
      type: Sequelize.FLOAT,
      validate:{
        min: 2.00,
        max: 5.00
      }
    },
    user_name: {
          type: Sequelize.STRING
      }
  });

  return Admission;
};
