const { authJwt } = require("../middleware");
const controller = require("../controllers/admission.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
      "/api/auth/admission",
      [authJwt.verifyToken],
      controller.admission
  );

  app.get(
      "/api/auth/admission",
      [authJwt.verifyToken],
      controller.getAdmission
  );
};
