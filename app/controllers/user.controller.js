const jwt = require("jsonwebtoken");
const db = require("../models");
const User = db.user;

exports.allAccess = (req, res) => {  
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    let tokenDecoded = jwt.decode(req.headers["x-access-token"]);
    User.findByPk(tokenDecoded["id"])
        .then(data => {
            if (data) {
                console.log("user name  : " + data["username"]);
                res.status(200).send("User Content of user " + data["username"]);
            }
        });
  };

// exports.adminBoard = (req, res) => {
//   res.status(200).send("Admin Content.");
// };

// exports.moderatorBoard = (req, res) => {
//   res.status(200).send("Moderator Content.");
// };
