const db = require("../models");
const Admission = db.admission;
const jwt = require("jsonwebtoken");
const User = db.user;

const Op = db.Sequelize.Op;

exports.admission = (req, res) => {

    let tokenDecoded = jwt.decode( req.headers["x-access-token"]);
    User.findByPk(tokenDecoded["id"])
        .then(data => {
            if (data) {
                console.log("user name  : " + data["username"]);
                let userName = data["username"];
                //---refactor : duplicate Check
                Admission.findOne({
                    where: {
                        user_name: userName
                    }
                }).then(admission => {
                    if (admission) {
                        return res.status(200).send({ message: "Admission Al-Ready Sudmitted!" });
                    } else {
                        //---------refactor : Have to move in another function
                        Admission.create({
                            full_name: req.body.full_name,
                            mobile: req.body.mobile,
                            dept: req.body.dept,
                            ssc_point: req.body.ssc_point,
                            hsc_point: req.body.hsc_point,
                            user_name: userName
                        }).then(admission => {
                            if (admission) {
                                res.send({ message: "Admission successful!" });
                            } else {
                                res.send({ message: "Admission failed!" });
                            }
                        }).catch(err => {
                                res.status(500).send({ message: err.message });
                        });
                        //---------
                       }
                    }).catch(err => {
                        res.status(500).send({ message: err.message });
                    });
                //----
            }
        });
};

exports.getAdmission = (req, res) => {

    let tokenDecoded = jwt.decode( req.headers["x-access-token"]);
    User.findByPk(tokenDecoded["id"])
        .then(data => {
            if (data) {
                console.log("user name get Admission : " + data["username"]);
                let userName = data["username"];
                Admission.findOne({
                    where: {
                        user_name: userName
                    }
                }).then(admission => {
                    console.log(admission);
                    if (admission) {
                        return res.status(200).send(admission);
                    } else {
                        return res.status(404).send(false);
                    }
                }).catch(err => {
                    res.status(500).send({ message: err.message });
                });
                //----
            }
        });
};



